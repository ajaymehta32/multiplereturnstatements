package ajaymehta.multiplereturnstatements;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program {

    static int a =5 , b=7 ,d =8;

    public static int bigger() {

        if(a>b) return a;

        if(a<b) return b;  // the program will stop here ..coz here it satisfied the condition... n method will retun value ..it will not go further from here..come out of method here.

        if(a>d) return a;

        if(a<d) return d;

    //TODo ...uncommnet n see if you dont have return statement here it will give u error n ask for return statement..
        return 0;  // above return statements with if ..can't make up for the main return statemnt ..so we have to put return statemetn .if all if conditions fail..then it will execute..
    }



    public static void main(String args[]) {

        System.out.println(bigger());


    }
}
