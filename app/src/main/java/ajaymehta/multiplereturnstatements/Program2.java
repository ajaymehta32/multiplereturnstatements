package ajaymehta.multiplereturnstatements;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program2 {

    static int a =5 , b=7 ,d =8;

    public static String bigger() {

        if(a>b) return "a is bigger";

        if(a<b) return "b is bigger";  // program stops here.. n come out of method n return value
        if(a>d) return "a is bigger";

        if(a<d) return "d is bigger";


        return null;
    }



    public static void main(String args[]) {

        System.out.println(bigger());


    }
}
